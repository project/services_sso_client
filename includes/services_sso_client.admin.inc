<?php

function services_sso_client_user_admin_form($form_state) {
  $form['services_sso_client_server_address'] = array(
    '#title' => t('Server address'),
    '#description' => t('The address of the server in the format of http://exammple.com/subdir without the trailing slash. The /subdir part is only necessary if you Services Drupal site is not on the root of the domain.'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('services_sso_client_server_address', ''),
  );
  $form['services_sso_client_server_endpoint'] = array(
    '#title' => t('Endpoint name'),
    '#description' => t('The name of the endpoint on the Services REST service. Example: authentication'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('services_sso_client_server_endpoint', ''),
  );

  // Profile editing settings
  $form['profile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile options'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['profile']['services_sso_client_only_external_account_editing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only use external profile for user account editing'),
    '#default_value' => variable_get('services_sso_client_only_external_account_editing', TRUE),
  );
  $form['profile']['services_sso_client_external_account_editing_url'] = array(
    '#type' => 'textfield',
    '#title' => t('External user editing address'),
    '#default_value' => variable_get('services_sso_client_external_account_editing_url', ''),
    '#description' => t('The url of the external account editing interface. Example: http://example.com/user/[uid]/edit. The [uid] portion will be replaced with the actual uid of the remote Drupal user account.'),
  );

  // Debugging settings
  $form['debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debugging options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['debug']['services_sso_client_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log debugging info to watchdog'),
    '#default_value' => variable_get('services_sso_client_debug', TRUE),
  );

  if (variable_get('services_sso_client_only_external_account_editing', TRUE) && !strlen(variable_get('services_sso_client_external_account_editing_url', ''))) {
    drupal_set_message(t('A valid account editing URL needs to be supplied when external account editing is enabled.'));
  }
  
  return system_settings_form($form);
}

function services_sso_client_user_admin_form_validate($form, &$form_state) {
  $base_url = $form_state['values']['services_sso_client_server_address'] . '/' . $form_state['values']['services_sso_client_server_endpoint'];
  $data = array(
    'username' => 'admin',
    'password' => 'ishouldnotwork',
  );
  $data = http_build_query($data, '', '&');
  $headers = array();

  $response = drupal_http_request($base_url . '/user/login', $headers, 'POST', $data);

  if ($response->code == '401' && stripos($response->error, 'Wrong username or password') !== FALSE) {
    // Success
    drupal_set_message(t('The provided SSO server address seems to be valid.'));
  }
  else {
    form_set_error('services_sso_client_server_address', t('The provided SSO server address does not seem like a valid Services 3.x (http://drupal.org/project/services) Drupal site. Please verify that the address is entered correctly. This is the message returned by the server: ') . $response->code . ' ' . $response->error);
  }
}