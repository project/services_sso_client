(function($) {
  $(document).ready(function(context) {
    if ($("body", window.parent.document).length > 0) {
      // Close all colorboxes on page.
      window.parent.$.colorbox.close();
    }
  });
})(jQuery);